#include "FileHelper.h"

/*
* function reads the buffer inside of a file and returns it as string
* if file path wasn't correct (no file was open) prints error and exits with code 1
*/
std::string FileHelper::readFileToString(const std::string fileName)
{
	std::ifstream file;
	std::stringstream strStream;
	std::string str;

	file.open(fileName); // open the input file

	if (file.is_open()) // check if the path was valid
	{
		strStream << file.rdbuf(); // read the file
		str = strStream.str(); // returns the content of the file

		file.close(); // close file
		return str;
	}

	else // if not valid throw an error and exit the code
	{
		std::cerr << "error when trying to open the file: invalid file path" << std::endl;
		_exit(1);
	}
}

/*
* fuction copies the value in input file into the output file
*/
void FileHelper::writeWordsToFile(const std::string inputFileName, const std::string outputFileName)
{
	writeStringToFile(readFileToString(inputFileName), outputFileName);
}

// function puts a string inside the file path
void FileHelper::writeStringToFile(const std::string str, const std::string fileName)
{
	std::ofstream outputFile;
	outputFile.open(fileName); // open the output file

	outputFile << str; // gets he string from the input file and puts it in the output file
	outputFile.close(); // close output file
}
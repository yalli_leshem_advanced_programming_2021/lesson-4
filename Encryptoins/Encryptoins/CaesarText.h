#pragma once
#include "ShiftText.h"

class CaesarText : public ShiftText
{
public:
	CaesarText(const std::string text, const bool isEnc) : ShiftText(text, 3, isEnc, caesarEncrpt) {}; // constructor
	~CaesarText() = default; // default deconstructor

	std::string encrypt();
	std::string decrypt();
};
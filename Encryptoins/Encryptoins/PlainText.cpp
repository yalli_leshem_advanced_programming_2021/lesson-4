#include "PlainText.h"

int PlainText::_numOfTexts = 0;

// constructor 
PlainText::PlainText(const std::string text, const int encription)
{
	this->_encryptionType = encription;
	this->text = text;
	this->isEncrypted = false;
	_numOfTexts++;
}

// getters
bool PlainText::isEnc() const {	return this->isEncrypted; }
std::string PlainText::getText() const { return this->text; }

// setters
void PlainText::setEnc(const bool encrypted) { this->isEncrypted = encrypted; }

std::ostream& operator << (std::ostream& out, const PlainText& plainText)
{
	switch (plainText._encryptionType)
	{
	case shiftEncrpt:
		out << "Shift \n";
		break;

	case caesarEncrpt:
		out << "Caesar \n";
		break;

	case substitutionEncrpt:
		out << "Substitution \n";
		break;

	default:
		break;
	}
	out << plainText.text;
	return out;
}

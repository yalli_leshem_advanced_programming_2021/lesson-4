#pragma once
#include "PlainText.h"
#include "CaesarText.h"
#include "ShiftText.h"
#include "SubstitutionText.h"
#include <iostream>
#include <string>

enum options1 { fromString = 1,  fromFile, numOfTexts, leave };
enum cipherOpt { shiftCipher = 1, caesarCipher, substitutionCipher };

void menu();
int printMenu();
int printCipherOptions();
int getChoise(const int minVal, const int maxVal);
void readFromString();
void readFromFile();
std::string getCipheredText(const std::string text, const bool isEnc);

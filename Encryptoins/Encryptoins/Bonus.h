#pragma once
#include "ShiftText.h"
#include "FileHelper.h"

class Bonus
{
private:
	std::string _encryptedText;
	int _key;

public:
	Bonus(const std::string encryptedFileName);

	void saveCorrectKey();
	void findAllKeys();
};

#include "ShiftText.h"

ShiftText::ShiftText(const std::string text, const int key, const bool isEnc, const int encryption) : PlainText(text, encryption), _key(key)
{
	setEnc(isEnc);
	encrypt();
}

// function encrypts the text (only if it's not encrypted already) by moving each letter in the text n amount of places (n being _key)
std::string ShiftText::encrypt()
{
	int letter;

	if (!isEncrypted) // if the text wasn't encrypted already
	{
		for (int i = 0; i < text.length(); i++)
		{
			letter = int(text[i]); // get the ascii value of the current letter
			
			// encrypt only letters in the alphabet (ignore non letters)
			if (letter <= (A_VALUE_IN_ASCII + LETTERS_IN_THE_ALPHABET) && A_VALUE_IN_ASCII <= letter)
			{
				letter -= A_VALUE_IN_ASCII; // get the position of the letter in the alphabet
				letter += this->_key; // add the key to the value
				letter %= LETTERS_IN_THE_ALPHABET; // makes sure the new value is still in the alphabet

				letter += A_VALUE_IN_ASCII; // return the letter to its ascii value

				text[i] = letter; // update the letter to its new value
			}
		}
		isEncrypted = true;
	}
	return text;
}

// function decrypts the text (only if it's encrypted) by moving each letter in the text -n amount of of places (n being _key) 
std::string ShiftText::decrypt()
{
	int letter;

	if (isEncrypted) // if the text is encrypted
	{
		for (int i = 0; i < text.length(); i++)
		{
			letter = int(text[i]); // get the ascii value of the current letter

			// decrypt only letters in the alphabet (ignore non letters)
			if (letter <= (A_VALUE_IN_ASCII + LETTERS_IN_THE_ALPHABET) && A_VALUE_IN_ASCII <= letter)
			{
				letter -= A_VALUE_IN_ASCII; // get the position of the letter in the alphabet
				letter += LETTERS_IN_THE_ALPHABET - this->_key; // when adding 26 - key, the text decrypts
				letter %= LETTERS_IN_THE_ALPHABET; // makes sure the new value is still in the alphabet

				letter += A_VALUE_IN_ASCII; // return the letter to its ascii value
				text[i] = letter; // update the letter to its new value
			}
		}
		isEncrypted = false;
	}
	return text;
}
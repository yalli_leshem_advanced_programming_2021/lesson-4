#pragma once
#include <iostream>
#include <string>

#define LETTERS_IN_THE_ALPHABET 26
#define A_VALUE_IN_ASCII 97

enum encryptionType { noEncrpt, shiftEncrpt, caesarEncrpt, substitutionEncrpt};

class PlainText
{
protected:
	std::string text;
	bool isEncrypted;
	int _encryptionType;
	static int _numOfTexts;

public:
	PlainText(const std::string text, const int encryption = noEncrpt); // constructor
	~PlainText() = default; // default deconstructor
	
	// getters
	bool isEnc() const;
	std::string getText() const;
	static int getCounter() { return _numOfTexts; }

	// setters
	void setEnc(const bool encrypted);

	friend std::ostream& operator << (std::ostream& out, const PlainText& plainText);
};
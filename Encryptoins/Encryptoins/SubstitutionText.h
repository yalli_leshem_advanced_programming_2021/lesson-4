#pragma once
#include "PlainText.h"
#include "FileHelper.h"

#define DECRYPTED_LETTER_INDEX 0
#define ENCRYPTED_LETTER_INDEX 1

class SubstitutionText : public PlainText
{
private:
	std::string _dictionaryFileName;
	int* _formatedDict;
	void formatDictionary();

public:
	SubstitutionText(const std::string text, const std::string dictionaryFileName, const bool isEnc); // constructor
	~SubstitutionText(); // deconstructor

	std::string encrypt();
	std::string decrypt();
};
#pragma once
#include "PlainText.h"

class ShiftText : public PlainText
{
private:
	int _key;

public:
	ShiftText(const std::string text, const int key, const bool isEnc, const int encryption = shiftEncrpt); // constructor
	~ShiftText() = default; // default deconstructor

	std::string encrypt(); 
	std::string decrypt();
};
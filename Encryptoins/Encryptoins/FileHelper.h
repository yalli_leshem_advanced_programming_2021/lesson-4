#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

class FileHelper
{
public:
	static std::string readFileToString(const std::string fileName);
	static void writeWordsToFile(const std::string inputFileName, const std::string outputFileName);
	static void writeStringToFile(const std::string str, const std::string fileName);
};

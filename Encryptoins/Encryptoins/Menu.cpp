#include "Menu.h"

void menu()
{
	int choise = 0;

	while (choise != leave)
	{
		choise = printMenu();
		
		switch (choise)
		{
		case fromString:
			readFromString();
			break;

		case fromFile:
			readFromFile();
			break;

		case numOfTexts:
			std::cout << "Number of texts: " << PlainText::getCounter() << std::endl;
			break;

		case leave:
			std::cout << "Bye Bye :)" << std::endl;
			break;
		default:
			break;
		}
	}
}

// function prints the menu options and returns the users choise
int printMenu()
{
	std::cout << "\n";
	std::cout << "1. Decrypt string" << std::endl;
	std::cout << "2. Encrypt/decrypt .txt file content" << std::endl;
	std::cout << "3. Print amount of texts made" << std::endl;
	std::cout << "4. Exit" << std::endl;

	return getChoise(fromString, leave);
}

// function prints the encryption options and returns the users choise
int printCipherOptions()
{
	std::cout << "\n";
	std::cout << "Encryption types:" << std::endl;
	std::cout << "1. Shift cipher" << std::endl;
	std::cout << "2. Caesar cipher" << std::endl;
	std::cout << "3. Substitution cipher" << std::endl;

	return getChoise(shiftCipher, substitutionCipher);
}

/*
* function gets a valid input from the user
*/
int getChoise(const int minVal, const int maxVal)
{
	int choise = 0;

	std::cout << "Please choose one of the options: ";
	std::cin >> choise;
	getchar(); // clean buffer

	while (!(minVal <= choise && choise <= maxVal)) // make sure user input is valid
	{
		std::cout << "Choise not valid! please try again: ";
		std::cin >> choise;
		getchar(); // clean buffer
	}

	return choise;
}

// function gets a string from the user and deciphers it
void readFromString()
{
	std::string text;

	std::cout << "Enter text to decrypt: " << std::endl;;
	std::getline(std::cin, text);

	std::cout << getCipheredText(text, true) << std::endl;
}

/* 
* function gets a file path from the user and deciphers it 
*/
void readFromFile()
{
	const int ENCRYPTED = 1;
	const int DECRYPTED = 2;

	const int SAVE_TO_FILE = 1;
	const int PRINT_TO_SCREEN = 2;

	std::string text;
	std::string fileName;
	bool isEnc = true;

	// get file path and the value inside of the file
	std::cout << "Enter the full path to the file you want to open: " << std::endl;
	std::getline(std::cin, fileName);

	text = FileHelper::readFileToString(fileName);

	// get if the user wants to encypt the file or decrypt it
	std::cout << "1. file is encrypted" << std::endl;
	std::cout << "2. file is decrypted" << std::endl;

	if (getChoise(ENCRYPTED, DECRYPTED) == DECRYPTED)
		isEnc = false;

	std::string decipheredText = getCipheredText(text, isEnc); // get new text

	// get if the user wants to save the text to a new file or print it to the screen
	std::cout << "1. save the text into a new file" << std::endl;
	std::cout << "2. print the text to the screen" << std::endl;

	if (SAVE_TO_FILE == getChoise(SAVE_TO_FILE, PRINT_TO_SCREEN)) // if user wants to save the text to a file
	{
		// get file path
		std::string outputFileName;
		std::cout << "Enter the full path of the file you want to save in: " << std::endl;
		std::getline(std::cin, outputFileName);

		FileHelper::writeStringToFile(decipheredText, outputFileName);
	}
	else
		std::cout << decipheredText << std::endl;
}

// function decrypts/encrypts the text in the method the user chooses and returns it
std::string getCipheredText(const std::string text, const bool isEnc)
{
	const int CAESAR_KEY = 3;
	std::string dictFileName;
	int key;
	int cipherOpt;

	cipherOpt = printCipherOptions();

	if (shiftCipher == cipherOpt)
	{
		// get the key from the user
		std::cout << "Enter the shift key: ";
		std::cin >> key;
		getchar(); // clean buffer

		// build new shift text object and return the new text value
		ShiftText cipherObject(text, key, isEnc);
		
		if (isEnc) // if user wanted to decipher the text
			cipherObject.decrypt();

		return cipherObject.getText();
	}
	else if (caesarCipher == cipherOpt)
	{
		// build new caesar text object
		CaesarText cipherObject(text, isEnc);

		if (isEnc) // if user wanted to decipher the text
			cipherObject.decrypt();

		return cipherObject.getText();
	}
	else
	{
		// get dictionary file path
		std::cout << "Enter the full path of the dictionary file: " << std::endl;
		std::getline(std::cin, dictFileName);

		// build new substitution text object
		SubstitutionText cipherObject(text, dictFileName, isEnc);

		if (isEnc) // if user wanted to decipher the text
			cipherObject.decrypt();

		return cipherObject.getText();
	}
}

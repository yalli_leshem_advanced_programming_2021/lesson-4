#include "CaesarText.h"

// encrypts ceasar ciphered text (each letter goes up by 3)
std::string CaesarText::encrypt()
{
	return ShiftText::encrypt();
}

// decrypts ceasar ciphered text (each letter goes down by 3)
std::string CaesarText::decrypt()
{
	return ShiftText::decrypt();
}
#include "Bonus.h"

Bonus::Bonus(const std::string encryptedFileName)
{
	this->_encryptedText = FileHelper::readFileToString(encryptedFileName);
	this->_key = 0;
}

void Bonus::saveCorrectKey()
{
	std::string path;

	std::cout << "enter the correct key: ";
	std::cin >> _key;

	ShiftText encrypted(this->_encryptedText, _key, true);
	encrypted.decrypt();

	std::cout << "enter the file path to save the full decrypted text in: " << std::endl;
	std::getline(std::cin, path);

	FileHelper::writeStringToFile(encrypted.getText(), path);
}

void Bonus::findAllKeys()
{
	std::string decryptedLine;

	for (int i = 0; i < LETTERS_IN_THE_ALPHABET; i++)
	{
		ShiftText decipherObjet(this->_encryptedText, i, true);
		decipherObjet.decrypt();

		decryptedLine = decipherObjet.getText();
		decryptedLine = decryptedLine.substr(0, decryptedLine.find("."));

		if (i < 10)
			std::cout << '0';
		std::cout << i << " - " << decryptedLine << std::endl;
	}
}
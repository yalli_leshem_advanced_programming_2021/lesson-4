#include "SubstitutionText.h"

SubstitutionText::SubstitutionText(const std::string text, const std::string dictionaryFileName, const bool isEnc) : PlainText(text, substitutionEncrpt), _dictionaryFileName(dictionaryFileName)
{
	this->_formatedDict = new int[LETTERS_IN_THE_ALPHABET];
	formatDictionary();
	setEnc(isEnc);
	encrypt();
}

SubstitutionText::~SubstitutionText()
{
	// free the array
	delete[] _formatedDict;
	_formatedDict = nullptr;
}

// encrypts the text by swapping the letter in the text to the letter in the dict that is replacing it
std::string SubstitutionText::encrypt()
{
	int letter;

	if (!isEncrypted) // if the text wasn't encrypted already
	{
		for (int i = 0; i < text.length(); i++)
		{
			letter = int(text[i]); // get the ascii value of the current letter

			// encrypt only letters in the alphabet (ignore non letters)
			if (letter <= (A_VALUE_IN_ASCII + LETTERS_IN_THE_ALPHABET) && A_VALUE_IN_ASCII <= letter)
			{
				letter -= A_VALUE_IN_ASCII; // get the position of the letter in the alphabet (it's also the index in the formated dict of the encrypted letters)

				text[i] = this->_formatedDict[letter] + A_VALUE_IN_ASCII; // update the letter to its new value
			}
		}
		isEncrypted = true;
	}
	return text;
}

std::string SubstitutionText::decrypt()
{
	int letter;
	int swappedDict[LETTERS_IN_THE_ALPHABET] = { 0 };

	if (isEncrypted) // if the text is encrypted
	{
		// make a swapped version of the dict (the index now is the encypted letter and the value is the decrypted letter)
		for (int i = 0; i < LETTERS_IN_THE_ALPHABET; i++)
		{
			letter = this->_formatedDict[i]; // get the encypted letter
			swappedDict[letter] = i; // in index that the encypted letter is equal to, put the index of the encypted letter (swaps between the index and the val)
		}

		for (int i = 0; i < text.length(); i++)
		{
			letter = int(text[i]); // get the ascii value of the current letter

			// decrypt only letters in the alphabet (ignore non letters)
			if (letter <= (A_VALUE_IN_ASCII + LETTERS_IN_THE_ALPHABET) && A_VALUE_IN_ASCII <= letter)
			{
				letter -= A_VALUE_IN_ASCII; // get the position of the letter in the alphabet (it's also the index in the swapped dict of the encrypted letters)

				text[i] = swappedDict[letter] + A_VALUE_IN_ASCII; // update the letter to its new value
			}
		}
		isEncrypted = false;
	}
	return text;
}

/*
* function formats the dict in a easier way to use 
* the index is the decrypted letter and the value in each index is the encypted letter
*/
void SubstitutionText::formatDictionary()
{
	const int NEXT_LETTER = 4; // each encrypted letter in the dict has 5 elements before the next one
	const int FIRST_ENCRYPTED_LETTER_INDEX = 2;
	
	std::string dict;
	int letterCounter = 0;
	int currLetter;
	FileHelper fileHelper;

	dict = fileHelper.readFileToString(this->_dictionaryFileName); // holds the content of the file

	for (int i = FIRST_ENCRYPTED_LETTER_INDEX; i < dict.length(); i += NEXT_LETTER, letterCounter++)
	{
			currLetter = int(dict[i]) - A_VALUE_IN_ASCII; // get the position of the letter in the alphabet
			this->_formatedDict[letterCounter] = currLetter;
	}
}